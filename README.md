# Sauna code challenge

## Ascii map path processing

```
mvn clean install
java -jar ./target/saunacodechallenge-asciimap.jar resource map1_basic_example  # accessing files in resources directory
or
java -jar ./target/saunacodechallenge-asciimap.jar "{path_to_map}" 
```

Check resources folder for more examples.