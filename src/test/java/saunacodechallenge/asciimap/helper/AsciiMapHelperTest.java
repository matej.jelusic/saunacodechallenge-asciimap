package saunacodechallenge.asciimap.helper;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import saunacodechallenge.asciimap.model.AsciiMap;
import saunacodechallenge.asciimap.model.Position;

import java.util.List;

public class AsciiMapHelperTest {

    @Test
    public void test_getValidNeighbourPosition() {
        AsciiMap asciiMap = Mockito.mock(AsciiMap.class);

        Mockito.when(asciiMap.getCurrentPosition()).thenReturn(new Position(3, 3));
        Mockito.when(asciiMap.getCurrentCharacterWithOffset(0, 1)).thenReturn(' ');
        Mockito.when(asciiMap.getCurrentCharacterWithOffset(0, -1)).thenReturn('|');
        Mockito.when(asciiMap.getCurrentCharacterWithOffset(1, 0)).thenReturn('+');
        Mockito.when(asciiMap.getCurrentCharacterWithOffset(-1, 0)).thenReturn('A');

        List<Position> validNeighbourPositions = AsciiMapHelper.getValidNeighbourPositions(asciiMap);

        Assertions.assertEquals(2, validNeighbourPositions.size());
        Assertions.assertIterableEquals(List.of(new Position(4, 3), new Position(2, 3)), validNeighbourPositions);
    }
}
