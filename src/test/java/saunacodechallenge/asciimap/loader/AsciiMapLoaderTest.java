package saunacodechallenge.asciimap.loader;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import saunacodechallenge.asciimap.exception.MandatoryCharacterMissingException;
import saunacodechallenge.asciimap.exception.MultipleCharactersNotAllowedException;
import saunacodechallenge.asciimap.model.AsciiMap;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class AsciiMapLoaderTest {
    AsciiMapLoader asciiMapLoader = new AsciiMapLoader();

    @Test
    public void loadFromFile_map1_success() {
        AsciiMap asciiMap = asciiMapLoader.loadMapFromResourceFile("map1_basic_example");

        System.out.println(asciiMap.toString());
        Assertions.assertEquals(
                "  @---A---+\n" +
                        "          |\n" +
                        "  x-B-+   C\n" +
                        "      |   |\n" +
                        "      +---+\n", asciiMap.toString());
    }

    @Test
    public void loadFromFile_map3_success() {
        AsciiMap asciiMap = asciiMapLoader.loadMapFromResourceFile("map3_letters_on_turns");

        System.out.println(asciiMap.toString());
        Assertions.assertEquals(
                "  @---A---+\n" +
                        "          |\n" +
                        "  x-B-+   |\n" +
                        "      |   |\n" +
                        "      +---C\n", asciiMap.toString());
    }

    @Test
    public void loadFromFile_map4_success() {
        AsciiMap asciiMap = asciiMapLoader.loadMapFromResourceFile("map4_duplicate_letters");

        System.out.println(asciiMap.toString());
        Assertions.assertEquals(
                "     +-O-N-+\0\0\n" +
                        "     |     |\0\0\n" +
                        "     |   +-I-+\n" +
                        " @-G-O-+ | | |\n" +
                        "     | | +-+ E\n" +
                        "     +-+     S\n" +
                        "             |\n" +
                        "             x\n", asciiMap.toString());
    }

    @Test
    public void loadFromFile_map5_success() {
        AsciiMap asciiMap = asciiMapLoader.loadMapFromResourceFile("map5_compact_space");

        System.out.println(asciiMap.toString());
        Assertions.assertEquals(
                " +-L-+\0\0\n" +
                        " |  +A-+\n" +
                        "@B+ ++ H\n" +
                        " ++    x\n", asciiMap.toString());
    }

    @Test
    public void loadFromFile_map6_error() {
        assertThrows(MandatoryCharacterMissingException.class, () -> asciiMapLoader.loadMapFromResourceFile("map6_no_start"));
    }

    @Test
    public void loadFromFile_map7_error() {
        assertThrows(MandatoryCharacterMissingException.class, () -> asciiMapLoader.loadMapFromResourceFile("map7_no_end"));
    }

    @Test
    public void loadFromFile_map8_error() {
        assertThrows(MultipleCharactersNotAllowedException.class, () -> asciiMapLoader.loadMapFromResourceFile("map8_multiple_starts"));
    }

    @Test
    public void loadFromFile_map9_error() {
        assertThrows(MultipleCharactersNotAllowedException.class, () -> asciiMapLoader.loadMapFromResourceFile("map9_multiple_ends"));
    }
}
