package saunacodechallenge.asciimap.processor;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import saunacodechallenge.asciimap.exception.BrokenPathException;
import saunacodechallenge.asciimap.model.AsciiMap;
import saunacodechallenge.asciimap.model.Position;
import saunacodechallenge.asciimap.processor.impl.*;

public class CharProcessorLocatorTest {

    @Test
    public void test_locateLetterCharProcessor() {
        testLocateCharProcessorWithExpectedCharProcessor('A', LetterCharProcessor.class);
    }

    @Test
    public void test_locateEndCharProcessor() {
        testLocateCharProcessorWithExpectedCharProcessor('x', EndCharProcessor.class);
    }

    @Test
    public void test_locateHorizontalPathCharProcessor() {
        testLocateCharProcessorWithExpectedCharProcessor('-', HorizontalPathCharProcessor.class);
    }

    @Test
    public void test_locateVerticalCharProcessor() {
        testLocateCharProcessorWithExpectedCharProcessor('|', VerticalCharProcessor.class);
    }

    @Test
    public void test_locateStartCharProcessor() {
        testLocateCharProcessorWithExpectedCharProcessor('@', StartCharProcessor.class);
    }

    @Test
    public void test_locateCharProcessor_brokenPathException() {
        Assertions.assertThrows(BrokenPathException.class, () -> locateCharProcessor(' '));
    }

    private void testLocateCharProcessorWithExpectedCharProcessor(char currentCharacter, Class<? extends CharProcessor> expectedCharProcessorClass) {
        CharProcessor charProcessor = locateCharProcessor(currentCharacter);
        Assertions.assertTrue(expectedCharProcessorClass.isInstance(charProcessor));
    }

    private CharProcessor locateCharProcessor(char currentCharacter) {
        AsciiMap asciiMap = Mockito.mock(AsciiMap.class);
        CharProcessorLocator charProcessorLocator = CharProcessorLocator.getInstance();

        Mockito.when(asciiMap.getCurrentCharacter()).thenReturn(currentCharacter);
        Mockito.when(asciiMap.getCurrentPosition()).thenReturn(new Position(0, 0));

        return charProcessorLocator.locateProcessor(asciiMap);
    }
}
