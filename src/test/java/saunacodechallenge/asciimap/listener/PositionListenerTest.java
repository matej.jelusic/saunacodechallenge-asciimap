package saunacodechallenge.asciimap.listener;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import saunacodechallenge.asciimap.listener.impl.LettersCollectorListener;
import saunacodechallenge.asciimap.listener.impl.PathCollectorListener;
import saunacodechallenge.asciimap.model.Position;

public class PositionListenerTest {

    @Test
    public void lettersCollectorListenerTest() {
        PositionListener lettersCollectorListener = new LettersCollectorListener();

        testingPath(lettersCollectorListener);

        Assertions.assertEquals("ABCD", lettersCollectorListener.toString());
    }

    @Test
    public void pathCollectorListenerTest() {
        PositionListener pathCollectorListener = new PathCollectorListener();

        testingPath(pathCollectorListener);

        Assertions.assertEquals("@|AB-C|DA+x", pathCollectorListener.toString());
    }

    private void testingPath(PositionListener lettersCollectorListener) {
        Position currentPosition = new Position(1, 3);
        char currentCharacter = '@';
        lettersCollectorListener.visitPosition(currentPosition, currentCharacter);

        currentPosition = new Position(1, 3);
        currentCharacter = '|';
        lettersCollectorListener.visitPosition(currentPosition, currentCharacter);

        currentPosition = new Position(2, 3);
        currentCharacter = 'A';
        lettersCollectorListener.visitPosition(currentPosition, currentCharacter);
        currentPosition = new Position(3, 3);
        currentCharacter = 'B';
        lettersCollectorListener.visitPosition(currentPosition, currentCharacter);
        currentPosition = new Position(3, 4);
        currentCharacter = '-';
        lettersCollectorListener.visitPosition(currentPosition, currentCharacter);

        currentPosition = new Position(3, 5);
        currentCharacter = 'C';
        lettersCollectorListener.visitPosition(currentPosition, currentCharacter);

        currentPosition = new Position(2, 5);
        currentCharacter = '|';
        lettersCollectorListener.visitPosition(currentPosition, currentCharacter);

        currentPosition = new Position(3, 4);
        currentCharacter = 'D';
        lettersCollectorListener.visitPosition(currentPosition, currentCharacter);

        currentPosition = new Position(2, 3);
        currentCharacter = 'A';
        lettersCollectorListener.visitPosition(currentPosition, currentCharacter);

        currentPosition = new Position(8, 3);
        currentCharacter = '+';
        lettersCollectorListener.visitPosition(currentPosition, currentCharacter);


        currentPosition = new Position(0, 0);
        currentCharacter = 'x';
        lettersCollectorListener.visitPosition(currentPosition, currentCharacter);
    }
}
