package saunacodechallenge.asciimap.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AsciiMapTest {

    @Test
    public void test_isHorizontal_true() {
        AsciiMap asciiMap = new AsciiMap();
        asciiMap.setCurrentPosition(new Position(2, 5));
        asciiMap.setPreviousPosition(new Position(2, 6));

        Assertions.assertTrue(asciiMap.isHorizontalDirection());
    }

    @Test
    public void test_isHorizontal_false() {
        AsciiMap asciiMap = new AsciiMap();
        asciiMap.setCurrentPosition(new Position(2, 5));
        asciiMap.setPreviousPosition(new Position(1, 5));

        Assertions.assertFalse(asciiMap.isHorizontalDirection());
    }

    @Test
    public void test_isHorizontalWithNullPositions_false() {
        AsciiMap asciiMap = new AsciiMap();
        asciiMap.setCurrentPosition(new Position(2, 5));

        Assertions.assertFalse(asciiMap.isHorizontalDirection());
    }

    @Test
    public void test_isVertical_true() {
        AsciiMap asciiMap = new AsciiMap();
        asciiMap.setCurrentPosition(new Position(3, 5));
        asciiMap.setPreviousPosition(new Position(2, 5));

        Assertions.assertTrue(asciiMap.isVerticalDirection());
    }

    @Test
    public void test_isVertical_false() {
        AsciiMap asciiMap = new AsciiMap();
        asciiMap.setCurrentPosition(new Position(2, 5));
        asciiMap.setPreviousPosition(new Position(2, 6));

        Assertions.assertFalse(asciiMap.isVerticalDirection());
    }

    @Test
    public void test_isVerticalWithNullPositions_false() {
        AsciiMap asciiMap = new AsciiMap();
        asciiMap.setCurrentPosition(new Position(2, 5));

        Assertions.assertFalse(asciiMap.isVerticalDirection());
    }

    @Test
    public void test_getCurrentCharacter_success() {
        AsciiMap asciiMap = new AsciiMap();
        char[][] map = new char[5][5];
        char expectedCharacter = 'A';
        map[3][2] = expectedCharacter;
        asciiMap.setMap(map);
        asciiMap.setCurrentPosition(new Position(2, 3));

        Assertions.assertEquals(expectedCharacter, asciiMap.getCurrentCharacter());
    }

    @Test
    public void test_getCurrentCharacterAtPosition_success() {
        AsciiMap asciiMap = new AsciiMap();
        char[][] map = new char[5][5];
        char expectedCharacter = 'A';
        map[3][2] = expectedCharacter;
        asciiMap.setMap(map);
        asciiMap.setCurrentPosition(new Position(1, 1));

        Assertions.assertEquals(expectedCharacter, asciiMap.getCurrentCharacterWithOffset(1, 2));
    }

    @Test
    public void test_next_success() {
        AsciiMap asciiMap = new AsciiMap();
        Position previousPosition = new Position(1, 1);
        asciiMap.setPreviousPosition(previousPosition);

        Position currentPosition = new Position(1, 2);
        asciiMap.setCurrentPosition(currentPosition);

        Position nextPosition = new Position(2, 2);
        asciiMap.next(nextPosition);

        Assertions.assertEquals(nextPosition, asciiMap.getCurrentPosition());
        Assertions.assertEquals(currentPosition, asciiMap.getPreviousPosition());
    }
}
