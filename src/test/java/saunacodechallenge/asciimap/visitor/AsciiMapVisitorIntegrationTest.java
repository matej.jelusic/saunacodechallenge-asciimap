package saunacodechallenge.asciimap.visitor;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import saunacodechallenge.asciimap.exception.*;
import saunacodechallenge.asciimap.listener.impl.LettersCollectorListener;
import saunacodechallenge.asciimap.listener.impl.PathCollectorListener;
import saunacodechallenge.asciimap.loader.AsciiMapLoader;
import saunacodechallenge.asciimap.model.AsciiMap;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class AsciiMapVisitorIntegrationTest {
    private AsciiMapLoader asciiMapLoader = new AsciiMapLoader();
    private AsciiMapVisitor asciiMapVisitor;
    private LettersCollectorListener lettersCollectorListener;
    private PathCollectorListener pathCollectorListener;


    @BeforeEach
    public void initialize() {
        asciiMapVisitor = new AsciiMapVisitor();
        lettersCollectorListener = new LettersCollectorListener();
        pathCollectorListener = new PathCollectorListener();

        asciiMapVisitor.addPositionListener(lettersCollectorListener);
        asciiMapVisitor.addPositionListener(pathCollectorListener);
    }

    @Test
    public void map1_basic_example_success() {
        visitAsciiMap("map1_basic_example", "ACB", "@---A---+|C|+---+|+-B-x");
    }

    @Test
    public void map2_straight_through_intersections_success() {
        visitAsciiMap("map2_straight_through_intersections", "ABCD", "@|A+---B--+|+--C-+|-||+---D--+|x");
    }

    @Test
    public void map3_letters_on_turns_success() {
        visitAsciiMap("map3_letters_on_turns", "ACB", "@---A---+|||C---+|+-B-x");
    }

    @Test
    public void map4_duplicate_letters_success() {
        visitAsciiMap("map4_duplicate_letters", "GOONIES", "@-G-O-+|+-+|O||+-O-N-+|I|+-+|+-I-+|ES|x");
    }

    @Test
    public void map5_compact_space_success() {
        visitAsciiMap("map5_compact_space", "BLAH", "@B+++B|+-L-+A+++A-+Hx");
    }

    @Test
    public void map6_no_start_success() {
        visitAsciiMap("map6_no_start", MandatoryCharacterMissingException.class);
    }

    @Test
    public void map7_no_end_characterMissingException() {
        visitAsciiMap("map7_no_end", MandatoryCharacterMissingException.class);
    }

    @Test
    public void map8_multiple_starts_multipleCharactersNotAllowedException() {
        visitAsciiMap("map8_multiple_starts", MultipleCharactersNotAllowedException.class);
    }

    @Test
    public void map9_multiple_ends_multipleCharactersNotAllowedException() {
        visitAsciiMap("map9_multiple_ends", MultipleCharactersNotAllowedException.class);
    }

    @Test
    public void map10_forks_invalidAsciiMapException() {
        visitAsciiMap("map10_forks", MultipleCharactersNotAllowedException.class);
    }

    @Test
    public void map11_broken_path_brokenPathException() {
        visitAsciiMap("map11_broken_path", BrokenPathException.class);
    }

    @Test
    public void map12_multiple_starting_paths_multipleStartingPathsException() {
        visitAsciiMap("map12_multiple_starting_paths", MultipleStartingPathsException.class);
    }

    @Test
    public void map13_fake_turn_invalidAsciiMapException() {
        visitAsciiMap("map13_fake_turn", FakeTurnException.class);
    }

    private void visitAsciiMap(String path, String expectedLettersCollectorResult, String expectedPathCollectorResult) {
        AsciiMap asciiMap = asciiMapLoader.loadMapFromResourceFile(path);
        asciiMapVisitor.visitAsciMap(asciiMap);

        Assertions.assertEquals(expectedLettersCollectorResult, lettersCollectorListener.toString());
        Assertions.assertEquals(expectedPathCollectorResult, pathCollectorListener.toString());
    }

    public void visitAsciiMap(String path, Class<? extends RuntimeException> exception) {
        assertThrows(exception, () -> {
            AsciiMap asciiMap = asciiMapLoader.loadMapFromResourceFile(path);
            asciiMapVisitor.visitAsciMap(asciiMap);
        });
    }

}
