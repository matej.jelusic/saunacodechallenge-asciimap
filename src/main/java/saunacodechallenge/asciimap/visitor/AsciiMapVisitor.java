package saunacodechallenge.asciimap.visitor;

import saunacodechallenge.asciimap.listener.PositionListener;
import saunacodechallenge.asciimap.model.AsciiMap;
import saunacodechallenge.asciimap.model.Constants;
import saunacodechallenge.asciimap.model.Position;
import saunacodechallenge.asciimap.processor.CharProcessor;
import saunacodechallenge.asciimap.processor.CharProcessorLocator;

import java.util.LinkedList;
import java.util.List;

public class AsciiMapVisitor {
    private final List<PositionListener> positionListeners = new LinkedList<>();
    private final CharProcessorLocator charProcessorLocator = CharProcessorLocator.getInstance();

    public void visitAsciMap(AsciiMap asciiMap) {
        while (Constants.END_CHAR != asciiMap.getPreviousCharacter()) {
            CharProcessor charProcessor = charProcessorLocator.locateProcessor(asciiMap);

            positionListeners.forEach(positionListener -> positionListener.visitPosition(asciiMap.getCurrentPosition(), asciiMap.getCurrentCharacter()));
            Position nextPosition = charProcessor.next(asciiMap);
            asciiMap.next(nextPosition);
        }
    }

    public void addPositionListener(PositionListener positionListener) {
        positionListeners.add(positionListener);
    }
}
