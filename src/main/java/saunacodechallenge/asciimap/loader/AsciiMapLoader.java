package saunacodechallenge.asciimap.loader;

import saunacodechallenge.asciimap.exception.MandatoryCharacterMissingException;
import saunacodechallenge.asciimap.exception.MultipleCharactersNotAllowedException;
import saunacodechallenge.asciimap.model.AsciiMap;
import saunacodechallenge.asciimap.model.Constants;
import saunacodechallenge.asciimap.model.Position;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public class AsciiMapLoader {

    public AsciiMap loadMapFromResourceFile(String path) {
        ClassLoader classLoader = AsciiMapLoader.class.getClassLoader();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(Objects.requireNonNull(classLoader.getResourceAsStream(path))));
        return loadMapFromList(bufferedReader.lines().collect(Collectors.toList()));
    }

    public AsciiMap loadMapFromFile(String path) {
        File file = new File(path);
        List<String> lines;
        try {
            lines = Files.readAllLines(Paths.get(file.getAbsolutePath()));
        } catch (IOException e) {
            throw new RuntimeException("Error loading file: " + file.getAbsolutePath());
        }
        return loadMapFromList(lines);
    }

    private AsciiMap loadMapFromList(List<String> lines) {
        int columns = lines.stream().mapToInt(String::length).max().orElseThrow();
        int rows = lines.size();

        char[][] map = new char[columns][rows];
        AsciiMap asciiMap = new AsciiMap();
        int row = 0;
        for (String line : lines) {
            int column = 0;
            for (char character : line.toCharArray()) {
                checkIfEmptyAndSet(character, asciiMap, AsciiMap::getStartingPosition, AsciiMap::setStartingPosition, column, row, Constants.START_CHAR);
                checkIfEmptyAndSet(character, asciiMap, AsciiMap::getEndingPosition, AsciiMap::setEndingPosition, column, row, Constants.END_CHAR);

                map[column][row] = character == '\0' ? ' ' : character;
                column++;
            }
            row++;
        }

        validateStartingAndEndingPosition(asciiMap);

        asciiMap.setMap(map);
        asciiMap.setCurrentPosition(asciiMap.getStartingPosition());
        return asciiMap;
    }

    private void validateStartingAndEndingPosition(AsciiMap asciiMap) {
        if (asciiMap.getStartingPosition() == null) {
            throw new MandatoryCharacterMissingException(Constants.START_CHAR);
        }
        if (asciiMap.getEndingPosition() == null) {
            throw new MandatoryCharacterMissingException(Constants.END_CHAR);
        }
    }

    private void checkIfEmptyAndSet(char currentCharacter, AsciiMap asciiMap, Function<AsciiMap, Position> getPosition,
                                    BiConsumer<AsciiMap, Position> setPosition, int column, int row, char expectedCharacter) {
        if (expectedCharacter == currentCharacter) {
            Position position = new Position(row, column);
            if (getPosition.apply(asciiMap) != null) {
                throw new MultipleCharactersNotAllowedException(currentCharacter);
            } else {
                setPosition.accept(asciiMap, position);
            }
        }
    }
}
