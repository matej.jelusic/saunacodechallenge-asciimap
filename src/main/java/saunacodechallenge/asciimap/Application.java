package saunacodechallenge.asciimap;

import saunacodechallenge.asciimap.listener.impl.LettersCollectorListener;
import saunacodechallenge.asciimap.listener.impl.PathCollectorListener;
import saunacodechallenge.asciimap.loader.AsciiMapLoader;
import saunacodechallenge.asciimap.model.AsciiMap;
import saunacodechallenge.asciimap.visitor.AsciiMapVisitor;


public class Application {

    public static void main(String[] args) {
        AsciiMap asciiMap = loadAsciiMap(args);

        AsciiMapVisitor asciiMapVisitor = new AsciiMapVisitor();

        LettersCollectorListener lettersCollectorListener = new LettersCollectorListener();
        PathCollectorListener pathCollectorListener = new PathCollectorListener();

        asciiMapVisitor.addPositionListener(lettersCollectorListener);
        asciiMapVisitor.addPositionListener(pathCollectorListener);

        asciiMapVisitor.visitAsciMap(asciiMap);
        System.out.println(lettersCollectorListener);
        System.out.println(pathCollectorListener);
    }

    private static AsciiMap loadAsciiMap(String[] args) {
        AsciiMapLoader asciiMapLoader = new AsciiMapLoader();
        AsciiMap asciiMap;
        if (args.length == 2 && "resource".equals(args[0])) {
            asciiMap = asciiMapLoader.loadMapFromResourceFile(args[1]);
        } else {
            asciiMap = asciiMapLoader.loadMapFromFile(args[0]);
        }
        return asciiMap;
    }
}
