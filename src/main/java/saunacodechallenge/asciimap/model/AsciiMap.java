package saunacodechallenge.asciimap.model;


import lombok.Data;

import java.util.Objects;

@Data
public class AsciiMap {
    private char[][] map;
    private Position startingPosition;
    private Position endingPosition;
    private Position currentPosition;
    private Position previousPosition;


    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < map[0].length; i++) {
            for (char[] chars : map) {
                stringBuilder.append(chars[i]);
            }
            stringBuilder.append("\n");
        }

        return stringBuilder.toString();
    }

    public char getCurrentCharacter() {
        if (currentPosition == null) {
            return '\0';
        }
        return map[currentPosition.getColumn()][currentPosition.getRow()];
    }

    public char getPreviousCharacter() {
        if (previousPosition == null) {
            return '\0';
        }
        return map[previousPosition.getColumn()][previousPosition.getRow()];
    }

    public void next(Position nextPosition) {
        if (previousPosition == null) {
            previousPosition = new Position();
        }
        previousPosition = currentPosition.toBuilder().build();
        if (nextPosition != null) {
            currentPosition = nextPosition.toBuilder().build();
        }
    }

    public char getCharacterAtPosition(int row, int column) {
        if (row < 0 || column < 0 || row >= map[0].length || column >= map.length) {
            return '\0';
        }
        return map[column][row];
    }

    public char getCurrentCharacterWithOffset(int rowOffset, int columnOffset) {
        return getCharacterAtPosition(currentPosition.getRow() + rowOffset, currentPosition.getColumn() + columnOffset);
    }

    public boolean isHorizontalDirection() {
        return previousPosition != null && currentPosition != null && Objects.equals(previousPosition.getRow(), currentPosition.getRow());
    }

    public boolean isVerticalDirection() {
        return previousPosition != null && currentPosition != null && Objects.equals(previousPosition.getColumn(), currentPosition.getColumn());
    }
}
