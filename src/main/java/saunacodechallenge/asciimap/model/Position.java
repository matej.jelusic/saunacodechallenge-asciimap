package saunacodechallenge.asciimap.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class Position {
    private Integer row;
    private Integer column;

    public Position getPositionWithOffset(int offsetRow, int offsetColumn) {
        return new Position(row + offsetRow, column + offsetColumn);
    }
}
