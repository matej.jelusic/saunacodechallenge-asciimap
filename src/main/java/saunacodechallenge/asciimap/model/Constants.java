package saunacodechallenge.asciimap.model;

public class Constants {
    public static final char START_CHAR = '@';
    public static final char END_CHAR = 'x';
    public static final char HORIZONTAL_CHAR = '-';
    public static final char VERTICAL_CHAR = '|';
    public static final char TURN_CHAR = '+';
}
