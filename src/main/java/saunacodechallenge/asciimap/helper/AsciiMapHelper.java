package saunacodechallenge.asciimap.helper;

import saunacodechallenge.asciimap.model.AsciiMap;
import saunacodechallenge.asciimap.model.Constants;
import saunacodechallenge.asciimap.model.Position;

import java.util.LinkedList;
import java.util.List;

public class AsciiMapHelper {

    public static List<Position> getValidNeighbourPositions(AsciiMap asciiMap) {
        List<Position> nextPositions = new LinkedList<>();

        AsciiMapHelper.addNextPositionIfNotInvalidCharacter(asciiMap, nextPositions, 0, 1, Constants.VERTICAL_CHAR);
        AsciiMapHelper.addNextPositionIfNotInvalidCharacter(asciiMap, nextPositions, 0, -1, Constants.VERTICAL_CHAR);
        AsciiMapHelper.addNextPositionIfNotInvalidCharacter(asciiMap, nextPositions, 1, 0, Constants.HORIZONTAL_CHAR);
        AsciiMapHelper.addNextPositionIfNotInvalidCharacter(asciiMap, nextPositions, -1, 0, Constants.HORIZONTAL_CHAR);
        return nextPositions;
    }

    private static void addNextPositionIfNotInvalidCharacter(AsciiMap asciiMap, List<Position> nextPositions, int rowOffset, int columnOffset, char invalidCharacter) {
        Position currentPosition = asciiMap.getCurrentPosition();

        char currentCharacter = asciiMap.getCurrentCharacterWithOffset(rowOffset, columnOffset);
        if (currentCharacter != invalidCharacter && currentCharacter != '\0' && currentCharacter != ' ') {
            nextPositions.add(currentPosition.getPositionWithOffset(rowOffset, columnOffset));
        }
    }
}
