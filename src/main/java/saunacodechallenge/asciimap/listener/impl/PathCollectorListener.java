package saunacodechallenge.asciimap.listener.impl;

import saunacodechallenge.asciimap.listener.PositionListener;
import saunacodechallenge.asciimap.model.Position;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class PathCollectorListener implements PositionListener {
    List<Character> path = new LinkedList<>();

    @Override
    public void visitPosition(Position currentPosition, char ascii) {
        path.add(ascii);
    }

    @Override
    public String toString() {
        return path.stream().map(String::valueOf).collect(Collectors.joining());
    }
}
