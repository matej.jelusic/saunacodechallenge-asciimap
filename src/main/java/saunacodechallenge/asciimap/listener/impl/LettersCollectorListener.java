package saunacodechallenge.asciimap.listener.impl;

import saunacodechallenge.asciimap.listener.PositionListener;
import saunacodechallenge.asciimap.model.Position;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class LettersCollectorListener implements PositionListener {
    private final Map<Position, Character> characterByPosition = new LinkedHashMap<>();

    @Override
    public void visitPosition(Position currentPosition, char ascii) {
        if (Character.isLetter(ascii) && ascii != 'x') {
            characterByPosition.put(currentPosition, ascii);
        }
    }

    @Override
    public String toString() {
        return characterByPosition.values().stream().map(String::valueOf).collect(Collectors.joining());
    }
}
