package saunacodechallenge.asciimap.listener;

import saunacodechallenge.asciimap.model.Position;

public interface PositionListener {

    void visitPosition(Position currentPosition, char ascii);
}
