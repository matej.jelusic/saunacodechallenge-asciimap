package saunacodechallenge.asciimap.exception;

import saunacodechallenge.asciimap.model.Position;

public class FakeTurnException extends RuntimeException {

    public FakeTurnException(Position position) {
        super(String.format("Position in row %s and column %s contains fake turn", position.getRow(), position.getColumn()));
    }
}
