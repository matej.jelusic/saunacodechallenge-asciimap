package saunacodechallenge.asciimap.exception;

import saunacodechallenge.asciimap.model.Position;

public class InvalidAsciiMapException extends RuntimeException {

    public InvalidAsciiMapException(Position position, Character character) {
        super("Invalid character: " + character + " at position: " + position.toString());
    }
}
