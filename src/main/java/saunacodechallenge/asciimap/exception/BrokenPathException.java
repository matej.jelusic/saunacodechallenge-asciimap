package saunacodechallenge.asciimap.exception;

import saunacodechallenge.asciimap.model.Position;

public class BrokenPathException extends RuntimeException {

    public BrokenPathException(Position position, char character) {
        super(String.format("Broken path at position: %s. Invalid character found: %s", position.toString(), character));
    }
}
