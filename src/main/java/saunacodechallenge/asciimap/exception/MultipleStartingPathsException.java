package saunacodechallenge.asciimap.exception;

import saunacodechallenge.asciimap.model.Position;

import java.util.List;
import java.util.stream.Collectors;

public class MultipleStartingPathsException extends RuntimeException {

    public MultipleStartingPathsException(Position startingPosition, List<Position> startingPathPositions) {
        super(String.format("Start at position: %s has multiple starting paths with positions:  %s", startingPosition.toString(), startingPathPositions.stream().map(Position::toString).collect(Collectors.joining())));
    }
}
