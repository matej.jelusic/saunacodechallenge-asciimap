package saunacodechallenge.asciimap.exception;

import saunacodechallenge.asciimap.model.Position;

public class TForkNotAllowedException extends RuntimeException {

    public TForkNotAllowedException(Position position) {
        super(String.format("T fork is not allowed. Invalid turn position: %s", position.toString()));
    }
}
