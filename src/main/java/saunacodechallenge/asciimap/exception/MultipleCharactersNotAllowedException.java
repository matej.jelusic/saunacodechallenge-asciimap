package saunacodechallenge.asciimap.exception;

public class MultipleCharactersNotAllowedException extends RuntimeException {

    public MultipleCharactersNotAllowedException(char character) {
        super(character + " is allowed only once");
    }
}
