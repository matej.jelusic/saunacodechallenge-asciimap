package saunacodechallenge.asciimap.exception;

public class MandatoryCharacterMissingException extends RuntimeException {

    public MandatoryCharacterMissingException(char character) {
        super(character + " is missing in map.");
    }
}
