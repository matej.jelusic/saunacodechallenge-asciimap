package saunacodechallenge.asciimap.processor;

import saunacodechallenge.asciimap.model.AsciiMap;
import saunacodechallenge.asciimap.model.Position;

public interface CharProcessor {
    Position next(AsciiMap asciiMap);
}
