package saunacodechallenge.asciimap.processor;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import saunacodechallenge.asciimap.exception.BrokenPathException;
import saunacodechallenge.asciimap.model.AsciiMap;
import saunacodechallenge.asciimap.model.Constants;
import saunacodechallenge.asciimap.processor.impl.*;

import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CharProcessorLocator {
    private static final Map<Character, CharProcessor> charProcessorByCharacter = new HashMap<>();
    private static final LetterCharProcessor letterCharProcessor = LetterCharProcessor.getInstance();

    private static CharProcessorLocator instance;

    public static CharProcessorLocator getInstance() {
        if (instance == null) {
            instance = new CharProcessorLocator();
        }
        return instance;
    }

    static {
        charProcessorByCharacter.put(Constants.START_CHAR, StartCharProcessor.getInstance());
        charProcessorByCharacter.put(Constants.END_CHAR, EndCharProcessor.getInstance());
        charProcessorByCharacter.put(Constants.VERTICAL_CHAR, VerticalCharProcessor.getInstance());
        charProcessorByCharacter.put(Constants.HORIZONTAL_CHAR, HorizontalPathCharProcessor.getInstance());
        charProcessorByCharacter.put(Constants.TURN_CHAR, TurnCharProcessor.getInstance());
    }

    public CharProcessor locateProcessor(AsciiMap asciiMap) {
        if (Character.isLetter(asciiMap.getCurrentCharacter()) && asciiMap.getCurrentCharacter() != 'x') {
            return letterCharProcessor;
        }
        CharProcessor charProcessor = charProcessorByCharacter.get(asciiMap.getCurrentCharacter());

        if (charProcessor == null) {
            throw new BrokenPathException(asciiMap.getCurrentPosition(), asciiMap.getCurrentCharacter());
        }
        return charProcessor;
    }
}
