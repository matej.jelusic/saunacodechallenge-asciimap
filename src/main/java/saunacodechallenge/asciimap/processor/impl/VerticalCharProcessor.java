package saunacodechallenge.asciimap.processor.impl;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import saunacodechallenge.asciimap.model.AsciiMap;
import saunacodechallenge.asciimap.model.Position;
import saunacodechallenge.asciimap.processor.CharProcessor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class VerticalCharProcessor implements CharProcessor {
    private static VerticalCharProcessor instance;

    public static VerticalCharProcessor getInstance() {
        if (instance == null) {
            instance = new VerticalCharProcessor();
        }
        return instance;
    }

    @Override
    public Position next(AsciiMap asciiMap) {
        Integer previousRow = asciiMap.getPreviousPosition().getRow();
        Integer currentRow = asciiMap.getCurrentPosition().getRow();

        Integer nextRow = currentRow + currentRow - previousRow;

        return new Position(nextRow, asciiMap.getCurrentPosition().getColumn());
    }
}
