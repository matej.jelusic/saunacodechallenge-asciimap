package saunacodechallenge.asciimap.processor.impl;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import saunacodechallenge.asciimap.model.AsciiMap;
import saunacodechallenge.asciimap.model.Position;
import saunacodechallenge.asciimap.processor.CharProcessor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EndCharProcessor implements CharProcessor {
    private static EndCharProcessor instance;

    public static EndCharProcessor getInstance() {
        if (instance == null) {
            instance = new EndCharProcessor();
        }
        return instance;
    }

    @Override
    public Position next(AsciiMap asciiMap) {
        return null;
    }
}
