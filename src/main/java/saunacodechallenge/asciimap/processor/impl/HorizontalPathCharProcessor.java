package saunacodechallenge.asciimap.processor.impl;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import saunacodechallenge.asciimap.model.AsciiMap;
import saunacodechallenge.asciimap.model.Position;
import saunacodechallenge.asciimap.processor.CharProcessor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class HorizontalPathCharProcessor implements CharProcessor {

    private static HorizontalPathCharProcessor instance;

    public static HorizontalPathCharProcessor getInstance() {
        if (instance == null) {
            instance = new HorizontalPathCharProcessor();
        }
        return instance;
    }

    @Override
    public Position next(AsciiMap asciiMap) {
        if (asciiMap.isVerticalDirection()) {
            return VerticalCharProcessor.getInstance().next(asciiMap);
        }
        Integer previousColumn = asciiMap.getPreviousPosition().getColumn();
        Integer currentColumn = asciiMap.getCurrentPosition().getColumn();

        Integer nextColumn = currentColumn + currentColumn - previousColumn;

        return new Position(asciiMap.getCurrentPosition().getRow(), nextColumn);
    }
}
