package saunacodechallenge.asciimap.processor.impl;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import saunacodechallenge.asciimap.model.AsciiMap;
import saunacodechallenge.asciimap.model.Position;
import saunacodechallenge.asciimap.processor.CharProcessor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class LetterCharProcessor implements CharProcessor {
    private static LetterCharProcessor instance;

    public static LetterCharProcessor getInstance() {
        if (instance == null) {
            instance = new LetterCharProcessor();
        }
        return instance;
    }

    HorizontalPathCharProcessor horizontalPathCharProcessor = HorizontalPathCharProcessor.getInstance();
    VerticalCharProcessor verticalCharProcessor = VerticalCharProcessor.getInstance();
    TurnCharProcessor turnCharProcessor = TurnCharProcessor.getInstance();

    @Override
    public Position next(AsciiMap asciiMap) {
        Position nextPosition = null;
        if (asciiMap.isHorizontalDirection()) {
            nextPosition = horizontalPathCharProcessor.next(asciiMap);
        }

        if (asciiMap.isVerticalDirection() && (nextPosition == null || asciiMap.getCharacterAtPosition(nextPosition.getRow(), nextPosition.getColumn()) == '\0')) {
            nextPosition = verticalCharProcessor.next(asciiMap);
        }

        if ((nextPosition == null || asciiMap.getCharacterAtPosition(nextPosition.getRow(), nextPosition.getColumn()) == '\0')) {
            nextPosition = turnCharProcessor.next(asciiMap);
        }

        return nextPosition;
    }
}
