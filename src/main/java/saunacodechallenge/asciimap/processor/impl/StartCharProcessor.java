package saunacodechallenge.asciimap.processor.impl;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import saunacodechallenge.asciimap.exception.InvalidAsciiMapException;
import saunacodechallenge.asciimap.exception.MultipleStartingPathsException;
import saunacodechallenge.asciimap.helper.AsciiMapHelper;
import saunacodechallenge.asciimap.model.AsciiMap;
import saunacodechallenge.asciimap.model.Position;
import saunacodechallenge.asciimap.processor.CharProcessor;

import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class StartCharProcessor implements CharProcessor {
    private static StartCharProcessor instance;

    public static StartCharProcessor getInstance() {
        if (instance == null) {
            instance = new StartCharProcessor();
        }
        return instance;
    }

    @Override
    public Position next(AsciiMap asciiMap) {
        List<Position> nextPositions = AsciiMapHelper.getValidNeighbourPositions(asciiMap);

        if (nextPositions.size() == 0) {
            throw new InvalidAsciiMapException(asciiMap.getCurrentPosition(), asciiMap.getCurrentCharacter());
        }

        if (nextPositions.size() > 1) {
            throw new MultipleStartingPathsException(asciiMap.getCurrentPosition(), nextPositions);
        }

        return nextPositions.get(0);
    }


}
