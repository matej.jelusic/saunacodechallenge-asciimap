package saunacodechallenge.asciimap.processor.impl;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import saunacodechallenge.asciimap.exception.FakeTurnException;
import saunacodechallenge.asciimap.exception.TForkNotAllowedException;
import saunacodechallenge.asciimap.helper.AsciiMapHelper;
import saunacodechallenge.asciimap.model.AsciiMap;
import saunacodechallenge.asciimap.model.Position;
import saunacodechallenge.asciimap.processor.CharProcessor;

import java.util.List;
import java.util.Objects;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TurnCharProcessor implements CharProcessor {
    private static TurnCharProcessor instance;

    public static TurnCharProcessor getInstance() {
        if (instance == null) {
            instance = new TurnCharProcessor();
        }
        return instance;
    }

    @Override
    public Position next(AsciiMap asciiMap) {
        List<Position> nextPositions = AsciiMapHelper.getValidNeighbourPositions(asciiMap);
        nextPositions.remove(asciiMap.getPreviousPosition());

        if (nextPositions.size() != 1) {
            throw new TForkNotAllowedException(asciiMap.getCurrentPosition());
        }

        Position nextPosition = nextPositions.get(0);
        checkIfFakeTurn(asciiMap.getPreviousPosition(), asciiMap.getCurrentPosition(), nextPosition);

        return nextPosition;
    }

    private void checkIfFakeTurn(Position previousPosition, Position currentPosition, Position nextPosition) {
        if (Objects.equals(previousPosition.getRow(), nextPosition.getRow()) || Objects.equals(previousPosition.getColumn(), nextPosition.getColumn())) {
            throw new FakeTurnException(currentPosition);
        }
    }
}
